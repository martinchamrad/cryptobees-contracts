/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require("dotenv").config();
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-ganache");
// require("hardhat-ethernal");
// require("hardhat-contract-sizer");
const { API_URL, API_MAIN, PRIVATE_KEY, ETHERSCAN_API } = process.env;
module.exports = {
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  defaultNetwork: "ropsten",
  networks: {
    hardhat: {
      blockGasLimit: 30000000,
    },
    mainnet: {
      url: API_MAIN,
      accounts: [`0x${PRIVATE_KEY}`],
    },
    ropsten: {
      gas: 50000000,
      maxPriorityFeePerGas: 1999999987,
      url: API_URL,
      accounts: [`0x${PRIVATE_KEY}`],
    },
    gorli: {
      gas: 50000000,
      maxPriorityFeePerGas: 1999999987,
      url: API_URL,
      accounts: [`0x${PRIVATE_KEY}`],
    },
  },
  etherscan: {
    apiKey: ETHERSCAN_API,
  },
  gas: 50000000,
  maxPriorityFeePerGas: 1999999987,
  blockGasLimit: 30000000,
  contractSizer: {
    alphaSort: true,
    disambiguatePaths: false,
    runOnCompile: true,
    strict: true,
  },
};
